import { Router } from 'express';

import UserRouter from '@modules/users/infra/http/routes/user.routes';
import MovieRouter from '@modules/movies/infra/http/routes/movies.routes';
import DirectorRoutes from '@modules/directors/infra/http/routes/director.routes';

const routes = Router();

routes.use('/users', UserRouter);
routes.use('/movies', MovieRouter);
routes.use('/directors', DirectorRoutes);

export default routes;
