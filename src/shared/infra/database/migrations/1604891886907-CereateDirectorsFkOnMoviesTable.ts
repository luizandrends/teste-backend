import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey,
} from 'typeorm';

export default class CereateDirectorsFkOnMoviesTable1604891886907
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'movies',
      new TableColumn({
        name: 'director_id',
        type: 'uuid',
        isNullable: true,
        default: null,
      })
    );

    await queryRunner.createForeignKey(
      'movies',
      new TableForeignKey({
        name: 'MovieDirector',
        columnNames: ['director_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'directors',
        onDelete: 'SET NULL',
        onUpdate: 'CASCADE',
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('movies', 'MovieDirector');
    await queryRunner.dropColumn('movies', 'director_id');
  }
}
