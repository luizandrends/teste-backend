import { container } from 'tsyringe';

import '@modules/users/providers';

import IUsersInterface from '@modules/users/interfaces/IUsersInterface';
import UsersRepository from '@modules/users/infra/database/repositories/UsersRepository';

import IMoviesInterface from '@modules/movies/interfaces/IMoviesInterface';
import MoviesRepository from '@modules/movies/infra/database/repositories/MoviesRepositories';

import IDirectorsInterface from '@modules/directors/interfaces/IDirectorsInterface';
import DirectorsRepository from '@modules/directors/infra/database/repositories/DirectorsRepository';

container.registerSingleton<IUsersInterface>(
  'UsersRepository',
  UsersRepository
);

container.registerSingleton<IMoviesInterface>(
  'MoviesRepository',
  MoviesRepository
);

container.registerSingleton<IDirectorsInterface>(
  'DirectorsRepository',
  DirectorsRepository
);
