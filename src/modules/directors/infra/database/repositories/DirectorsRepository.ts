import { Repository, getRepository } from 'typeorm';

import IDirectorsInterface from '@modules/directors/interfaces/IDirectorsInterface';
import IDirectorDTO from '@modules/directors/dtos/IDirectorDTO';
import Director from '../entities/Director';

class DirectorsRepository implements IDirectorsInterface {
  private ormRepository: Repository<Director>;

  constructor() {
    this.ormRepository = getRepository(Director);
  }

  public async create(userData: IDirectorDTO): Promise<Director> {
    const user = this.ormRepository.create(userData);

    return user;
  }

  public async findByName(
    director_name: string
  ): Promise<Director | undefined> {
    const findDirector = await this.ormRepository.findOne({
      where: { name: director_name },
    });

    return findDirector;
  }

  public async findById(director_id: string): Promise<Director | undefined> {
    const findDirector = await this.ormRepository.findOne({
      where: { id: director_id },
    });

    return findDirector;
  }

  public async save(director: Director): Promise<Director> {
    return this.ormRepository.save(director);
  }
}

export default DirectorsRepository;
