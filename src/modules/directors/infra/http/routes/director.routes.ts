import { Router } from 'express';
import { celebrate, Segments, Joi } from 'celebrate';

import ensureAuthenticated from '@modules/users/infra/http/middlewares/ensureAuthenticated';
import ensureIsAdmin from '@modules/users/infra/http/middlewares/ensureIsAdmin';

import CreateDirectorController from '../controllers/CreateDirectorController';

const directorRouter = Router();

const createDirectorController = new CreateDirectorController();

directorRouter.post(
  '/create',
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required(),
    },
  }),
  ensureAuthenticated,
  ensureIsAdmin,
  createDirectorController.create
);

export default directorRouter;
