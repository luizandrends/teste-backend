import { container } from 'tsyringe';
import { Request, Response } from 'express';

import CreateDirectorService from '@modules/directors/services/CreateDirectorService';

class CreateDirectorController {
  public async create(request: Request, response: Response): Promise<Response> {
    const { name } = request.body;

    const createDirectorService = container.resolve(CreateDirectorService);

    const directorData = {
      name,
    };

    const director = await createDirectorService.execute(directorData);

    return response.json(director);
  }
}

export default CreateDirectorController;
