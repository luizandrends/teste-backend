import { injectable, inject } from 'tsyringe';

import AppError from '@shared/errors/AppError';

import Director from '../infra/database/entities/Director';

import IDirectorDTO from '../dtos/IDirectorDTO';
import IDirectorsInterface from '../interfaces/IDirectorsInterface';

@injectable()
class CreateDirectorService {
  constructor(
    @inject('DirectorsRepository')
    private directorsRepository: IDirectorsInterface
  ) {}

  public async execute(directorData: IDirectorDTO): Promise<Director> {
    const { name } = directorData;

    const findDirector = await this.directorsRepository.findByName(name);

    if (findDirector) {
      throw new AppError('Director already created', 400);
    }

    const createDirectorData = {
      name,
    };

    const director = await this.directorsRepository.create(createDirectorData);

    await this.directorsRepository.save(director);

    return director;
  }
}

export default CreateDirectorService;
