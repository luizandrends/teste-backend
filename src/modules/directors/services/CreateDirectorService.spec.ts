import AppError from '@shared/errors/AppError';
import FakeDirectorsRespository from '../interfaces/fakes/FakeDirectorsRespository';
import CreateDirectorService from './CreateDirectorService';

let fakeDirectorsRespository: FakeDirectorsRespository;
let createDirectorService: CreateDirectorService;

describe('CreateDirectorService', () => {
  beforeEach(() => {
    fakeDirectorsRespository = new FakeDirectorsRespository();

    createDirectorService = new CreateDirectorService(fakeDirectorsRespository);
  });

  it('should be able to create a new director', async () => {
    const directorData = {
      name: 'Quantin Tarantino',
    };

    const createUser = await createDirectorService.execute(directorData);

    expect(createUser).toHaveProperty('id');
  });

  it('should not be able to create an repeated director', async () => {
    const directorData = {
      name: 'Quantin tarantino',
    };

    await createDirectorService.execute(directorData);

    await expect(
      createDirectorService.execute(directorData)
    ).rejects.toBeInstanceOf(AppError);
  });
});
