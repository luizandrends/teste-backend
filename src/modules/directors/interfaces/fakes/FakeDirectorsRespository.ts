import { uuid } from 'uuidv4';

import Director from '@modules/directors/infra/database/entities/Director';
import IDirectorsInterface from '../IDirectorsInterface';
import IDirectorDTO from '../../dtos/IDirectorDTO';

class FakeDirectorsRespository implements IDirectorsInterface {
  private directors: Director[] = [];

  public async create(directorData: IDirectorDTO): Promise<Director> {
    const director = new Director();

    Object.assign(director, { id: uuid() }, directorData);

    this.directors.push(director);

    return director;
  }

  public async findByName(
    director_name: string
  ): Promise<Director | undefined> {
    const findDirector = this.directors.find(
      director => director.name === director_name
    );

    return findDirector;
  }

  public async findById(director_id: string): Promise<Director | undefined> {
    const findDirector = this.directors.find(
      director => director.id === director_id
    );

    return findDirector;
  }

  public async save(director: Director): Promise<Director> {
    const findIndex = this.directors.findIndex(
      findDirector => findDirector.id === director.id
    );

    this.directors[findIndex] = director;

    return director;
  }
}

export default FakeDirectorsRespository;
