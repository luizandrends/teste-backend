import IDirectorDTO from '../dtos/IDirectorDTO';
import Director from '../infra/database/entities/Director';

export default interface IDirectorsInterface {
  create(directorData: IDirectorDTO): Promise<Director>;
  findByName(director_name: string): Promise<Director | undefined>;
  findById(director_id: string): Promise<Director | undefined>;
  save(director: Director): Promise<Director>;
}
