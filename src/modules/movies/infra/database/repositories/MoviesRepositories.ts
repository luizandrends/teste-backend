import { Repository, getRepository } from 'typeorm';

import IMoviesInterface from '@modules/movies/interfaces/IMoviesInterface';
import IMovieDTO from '@modules/movies/dtos/IMovieDTO';
import Movie from '../entities/Movie';

class MoviesRepositories implements IMoviesInterface {
  private ormRepository: Repository<Movie>;

  constructor() {
    this.ormRepository = getRepository(Movie);
  }

  public async create(movieData: IMovieDTO): Promise<Movie> {
    const movie = this.ormRepository.create(movieData);

    return movie;
  }

  public async findById(movie_id: string): Promise<Movie | undefined> {
    const findMovie = this.ormRepository.findOne({
      where: { id: movie_id },
      relations: ['director'],
    });

    return findMovie;
  }

  public async findByName(movie_name: string): Promise<Movie | undefined> {
    const findMovie = this.ormRepository.findOne({
      where: { name: movie_name },
      relations: ['director'],
    });

    return findMovie;
  }

  public async findByDirectorId(
    director_id: string
  ): Promise<(Movie | undefined)[]> {
    const findMovies = this.ormRepository.find({
      where: { director_id },
      relations: ['director'],
    });

    return findMovies;
  }

  public async findByGender(gender: string): Promise<(Movie | undefined)[]> {
    const findMovies = this.ormRepository.find({
      where: { gender },
      relations: ['director'],
    });

    return findMovies;
  }

  public async save(movie: Movie): Promise<Movie> {
    return this.ormRepository.save(movie);
  }
}

export default MoviesRepositories;
