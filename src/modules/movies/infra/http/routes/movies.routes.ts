import { Router } from 'express';
import { celebrate, Segments, Joi } from 'celebrate';

import ensureAuthenticated from '@modules/users/infra/http/middlewares/ensureAuthenticated';
import ensureIsAdmin from '@modules/users/infra/http/middlewares/ensureIsAdmin';

import CreateMovieController from '../controllers/CreateMovieController';
import FindMovieByNameController from '../controllers/FindMovieByNameController';
import ListMoviesByDirectorController from '../controllers/ListMoviesByDirectorController';
import ListMoviesByGenderController from '../controllers/ListMoviesByGenderController';

const movieRouter = Router();

const createMovieController = new CreateMovieController();
const findMovieByNameController = new FindMovieByNameController();
const listMoviesByDirectorController = new ListMoviesByDirectorController();
const listMoviesByGenderController = new ListMoviesByGenderController();

movieRouter.post(
  '/create',
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required(),
      director_id: Joi.string().required(),
      gender: Joi.string().required(),
    },
  }),
  ensureAuthenticated,
  ensureIsAdmin,
  createMovieController.create
);

movieRouter.get(
  '/find/movie/:name',
  celebrate({
    [Segments.PARAMS]: {
      name: Joi.string().required(),
    },
  }),
  ensureAuthenticated,
  findMovieByNameController.find
);

movieRouter.get(
  '/list/director/:director_id',
  celebrate({
    [Segments.PARAMS]: {
      director_id: Joi.string().required(),
    },
  }),
  ensureAuthenticated,
  listMoviesByDirectorController.list
);

movieRouter.get(
  '/list/gender/:gender',
  celebrate({
    [Segments.PARAMS]: {
      gender: Joi.string().required(),
    },
  }),
  ensureAuthenticated,
  listMoviesByGenderController.list
);

export default movieRouter;
