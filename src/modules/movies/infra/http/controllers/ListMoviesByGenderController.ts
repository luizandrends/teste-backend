import { container } from 'tsyringe';
import { Request, Response } from 'express';

import ListMoviesByGenderService from '@modules/movies/services/ListMoviesByGenderService';

class ListMoviesByGenderController {
  public async list(request: Request, response: Response): Promise<Response> {
    const { gender } = request.params;

    const listMoviesByGenderService = container.resolve(
      ListMoviesByGenderService
    );

    const movies = await listMoviesByGenderService.execute(gender);

    return response.json(movies);
  }
}

export default ListMoviesByGenderController;
