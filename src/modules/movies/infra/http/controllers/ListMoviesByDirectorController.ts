import { container } from 'tsyringe';
import { Request, Response } from 'express';

import ListMoviesByDirectorService from '@modules/movies/services/ListMoviesByDirectorService';

class ListMoviesByDirectorController {
  public async list(request: Request, response: Response): Promise<Response> {
    const { director_id } = request.params;

    const listMoviesByDirectorService = container.resolve(
      ListMoviesByDirectorService
    );

    const movies = await listMoviesByDirectorService.execute(director_id);

    return response.json(movies);
  }
}

export default ListMoviesByDirectorController;
