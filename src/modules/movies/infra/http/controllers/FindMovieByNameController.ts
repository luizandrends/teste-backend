import { container } from 'tsyringe';
import { Request, Response } from 'express';

import FindMovieByNameService from '@modules/movies/services/FindMovieByNameService';

class FindMovieByNameController {
  public async find(request: Request, response: Response): Promise<Response> {
    const { name } = request.params;

    const findMovieByName = container.resolve(FindMovieByNameService);

    const movie = await findMovieByName.execute(name);

    return response.json(movie);
  }
}

export default FindMovieByNameController;
