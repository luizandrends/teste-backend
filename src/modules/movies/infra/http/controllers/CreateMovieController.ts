import { container } from 'tsyringe';
import { Request, Response } from 'express';

import CreateMovieService from '@modules/movies/services/CreateMovieService';

class CreateMovieController {
  public async create(request: Request, response: Response): Promise<Response> {
    const { name, director_id, gender } = request.body;

    const createMovie = container.resolve(CreateMovieService);

    const movieData = {
      name,
      director_id,
      gender,
    };

    const movie = await createMovie.execute(movieData);

    return response.json(movie);
  }
}

export default CreateMovieController;
