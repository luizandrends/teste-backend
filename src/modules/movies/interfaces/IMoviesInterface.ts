import Movie from '../infra/database/entities/Movie';
import IMovieDTO from '../dtos/IMovieDTO';

export default interface IMoviesInterface {
  create(movieData: IMovieDTO): Promise<Movie>;
  findById(movie_id: string): Promise<Movie | undefined>;
  findByName(movie_name: string): Promise<Movie | undefined>;
  findByDirectorId(director_id: string): Promise<(Movie | undefined)[]>;
  findByGender(gender: string): Promise<(Movie | undefined)[]>;
  save(movie: Movie): Promise<Movie>;
}
