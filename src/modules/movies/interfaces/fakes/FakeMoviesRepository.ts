import { uuid } from 'uuidv4';

import Movie from '@modules/movies/infra/database/entities/Movie';
import IMoviesInterface from '../IMoviesInterface';
import IMovieDTO from '../../dtos/IMovieDTO';

class FakeMoviesRepository implements IMoviesInterface {
  private movies: Movie[] = [];

  public async create(movieData: IMovieDTO): Promise<Movie> {
    const movie = new Movie();

    Object.assign(movie, { id: uuid() }, movieData);

    this.movies.push(movie);

    return movie;
  }

  public async findById(movie_id: string): Promise<Movie | undefined> {
    const findMovie = this.movies.find(movie => movie.id === movie_id);

    return findMovie;
  }

  public async findByName(movie_name: string): Promise<Movie | undefined> {
    const findMovie = this.movies.find(movie => movie.name === movie_name);

    return findMovie;
  }

  public async findByDirectorId(
    director_id: string
  ): Promise<(Movie | undefined)[]> {
    const listMovies = this.movies.map(movie => {
      return movie.director_id === director_id ? movie : undefined;
    });

    return listMovies;
  }

  public async findByGender(gender: string): Promise<(Movie | undefined)[]> {
    const listMovies = this.movies.map(movie => {
      return movie.gender === gender ? movie : undefined;
    });

    return listMovies;
  }

  public async save(movie: Movie): Promise<Movie> {
    const findIndex = this.movies.findIndex(
      findMovie => findMovie.id === movie.id
    );

    this.movies[findIndex] = movie;

    return movie;
  }
}

export default FakeMoviesRepository;
