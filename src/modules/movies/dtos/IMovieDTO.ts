export default interface IMovieDTO {
  name: string;
  director_id: string;
  gender: string;
}
