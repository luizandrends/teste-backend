import { injectable, inject } from 'tsyringe';

import AppError from '@shared/errors/AppError';

import Movie from '../infra/database/entities/Movie';

import IMoviesInterface from '../interfaces/IMoviesInterface';

@injectable()
class ListMoviesByGenderService {
  constructor(
    @inject('MoviesRepository')
    private moviesRepository: IMoviesInterface
  ) {}

  public async execute(gender: string): Promise<(Movie | undefined)[]> {
    const findMovie = await this.moviesRepository.findByGender(gender);

    if (!findMovie) {
      throw new AppError('Movie not found', 400);
    }

    return findMovie;
  }
}

export default ListMoviesByGenderService;
