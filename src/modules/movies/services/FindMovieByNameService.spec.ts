import AppError from '@shared/errors/AppError';

import FakeDirectorRepository from '@modules/directors/interfaces/fakes/FakeDirectorsRespository';
import CreateDirectorService from '@modules/directors/services/CreateDirectorService';

import FakeMoviesRepository from '../interfaces/fakes/FakeMoviesRepository';
import CreateMovieService from './CreateMovieService';

import FindMovieByNameService from './FindMovieByNameService';

let fakeDirectorRepository: FakeDirectorRepository;
let createDirectorService: CreateDirectorService;

let fakeMoviesRepository: FakeMoviesRepository;
let createMovieService: CreateMovieService;

let findMovieByNameService: FindMovieByNameService;

describe('FindMovieByNameService', () => {
  beforeEach(() => {
    fakeDirectorRepository = new FakeDirectorRepository();
    createDirectorService = new CreateDirectorService(fakeDirectorRepository);

    fakeMoviesRepository = new FakeMoviesRepository();
    createMovieService = new CreateMovieService(
      fakeMoviesRepository,
      fakeDirectorRepository
    );

    findMovieByNameService = new FindMovieByNameService(fakeMoviesRepository);
  });

  it('should be able to find a movie', async () => {
    const createDirectorData = {
      name: 'Quentin tarantino',
    };

    const createDirector = await createDirectorService.execute(
      createDirectorData
    );

    const createMovieData = {
      name: 'Inglorious bastards',
      director_id: createDirector.id,
      gender: 'Drama',
    };

    await createMovieService.execute(createMovieData);

    const findMovie = await findMovieByNameService.execute(
      'Inglorious bastards'
    );

    expect(findMovie).toHaveProperty('id');
  });

  it('should not be able to find an unexistent movie', async () => {
    const createDirectorData = {
      name: 'Quentin tarantino',
    };

    const director = await createDirectorService.execute(createDirectorData);

    const createMovieData = {
      name: 'Inglorious bastards',
      director_id: director.id,
      gender: 'Drama',
    };

    await createMovieService.execute(createMovieData);

    await expect(
      findMovieByNameService.execute('unexistent movie')
    ).rejects.toBeInstanceOf(AppError);
  });
});
