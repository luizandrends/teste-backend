import { injectable, inject } from 'tsyringe';

import AppError from '@shared/errors/AppError';

import Movie from '../infra/database/entities/Movie';

import IMoviesInterface from '../interfaces/IMoviesInterface';

@injectable()
class FindMovieByNameService {
  constructor(
    @inject('MoviesRepository')
    private moviesRepository: IMoviesInterface
  ) {}

  public async execute(movie_name: string): Promise<Movie | undefined> {
    const findMovie = await this.moviesRepository.findByName(movie_name);

    if (!findMovie) {
      throw new AppError('Movie not found', 400);
    }

    return findMovie;
  }
}

export default FindMovieByNameService;
