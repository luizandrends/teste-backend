import { injectable, inject } from 'tsyringe';

import IDirectorsInterface from '@modules/directors/interfaces/IDirectorsInterface';

import AppError from '@shared/errors/AppError';

import Movie from '../infra/database/entities/Movie';

import IMovieDTO from '../dtos/IMovieDTO';
import IMoviesInterface from '../interfaces/IMoviesInterface';

@injectable()
class CreateMovieService {
  constructor(
    @inject('MoviesRepository')
    private moviesRepository: IMoviesInterface,

    @inject('DirectorsRepository')
    private directorsRepository: IDirectorsInterface
  ) {}

  public async execute(userData: IMovieDTO): Promise<Movie> {
    const { name, director_id, gender } = userData;

    const findDirector = await this.directorsRepository.findById(director_id);

    if (!findDirector) {
      throw new AppError('Director not found', 400);
    }

    const createMovieData = {
      name,
      director_id,
      gender,
    };

    const movie = await this.moviesRepository.create(createMovieData);

    await this.moviesRepository.save(movie);

    return movie;
  }
}

export default CreateMovieService;
