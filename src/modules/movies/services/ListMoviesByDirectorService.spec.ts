import AppError from '@shared/errors/AppError';

import FakeDirectorRepository from '@modules/directors/interfaces/fakes/FakeDirectorsRespository';
import CreateDirectorService from '@modules/directors/services/CreateDirectorService';

import FakeMoviesRepository from '../interfaces/fakes/FakeMoviesRepository';
import CreateMovieService from './CreateMovieService';

import ListMoviesByDirectorService from './ListMoviesByDirectorService';

let fakeDirectorRepository: FakeDirectorRepository;
let createDirectorService: CreateDirectorService;

let fakeMoviesRepository: FakeMoviesRepository;
let createMovieService: CreateMovieService;

let listMoviesByDirectorService: ListMoviesByDirectorService;

describe('ListMoviesByDirectorService', () => {
  beforeEach(() => {
    fakeDirectorRepository = new FakeDirectorRepository();
    createDirectorService = new CreateDirectorService(fakeDirectorRepository);

    fakeMoviesRepository = new FakeMoviesRepository();
    createMovieService = new CreateMovieService(
      fakeMoviesRepository,
      fakeDirectorRepository
    );

    listMoviesByDirectorService = new ListMoviesByDirectorService(
      fakeMoviesRepository,
      fakeDirectorRepository
    );
  });

  it('should be able to list all movies', async () => {
    const createDirectorData = {
      name: 'Quentin tarantino',
    };

    const createDirector = await createDirectorService.execute(
      createDirectorData
    );

    const createFirstMovieData = {
      name: 'Inglorious bastards',
      director_id: createDirector.id,
      gender: 'Drama',
    };

    const createSecondMovieData = {
      name: 'Once umpon a time Hollywood',
      director_id: createDirector.id,
      gender: 'Drama',
    };

    const createFirstMovie = await createMovieService.execute(
      createFirstMovieData
    );
    const createSecondMovie = await createMovieService.execute(
      createSecondMovieData
    );

    const listMovies = await listMoviesByDirectorService.execute(
      createDirector.id
    );

    expect(listMovies).toEqual([createFirstMovie, createSecondMovie]);
  });

  it('should not be able to list the movies from an unexistent director', async () => {
    const createDirectorData = {
      name: 'Quentin tarantino',
    };

    const createDirector = await createDirectorService.execute(
      createDirectorData
    );

    const createFirstMovieData = {
      name: 'Inglorious bastards',
      director_id: createDirector.id,
      gender: 'Drama',
    };

    const createSecondMovieData = {
      name: 'Once umpon a time Hollywood',
      director_id: createDirector.id,
      gender: 'Drama',
    };

    await createMovieService.execute(createFirstMovieData);
    await createMovieService.execute(createSecondMovieData);

    await expect(
      listMoviesByDirectorService.execute('unexistent director')
    ).rejects.toBeInstanceOf(AppError);
  });
});
