import { injectable, inject } from 'tsyringe';

import IDirectorsInterface from '@modules/directors/interfaces/IDirectorsInterface';

import AppError from '@shared/errors/AppError';

import Movie from '../infra/database/entities/Movie';

import IMoviesInterface from '../interfaces/IMoviesInterface';

@injectable()
class ListMoviesByDirectorService {
  constructor(
    @inject('MoviesRepository')
    private moviesRepository: IMoviesInterface,

    @inject('DirectorsRepository')
    private directorsRepository: IDirectorsInterface
  ) {}

  public async execute(director_id: string): Promise<(Movie | undefined)[]> {
    const findDirector = await this.directorsRepository.findById(director_id);

    if (!findDirector) {
      throw new AppError('Director not found', 400);
    }

    const moviesList = await this.moviesRepository.findByDirectorId(
      director_id
    );

    return moviesList;
  }
}

export default ListMoviesByDirectorService;
