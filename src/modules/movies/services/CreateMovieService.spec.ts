import AppError from '@shared/errors/AppError';

import FakeDirectorRepository from '@modules/directors/interfaces/fakes/FakeDirectorsRespository';
import CreateDirectorService from '@modules/directors/services/CreateDirectorService';

import FakeMoviesRepository from '../interfaces/fakes/FakeMoviesRepository';
import CreateMovieService from './CreateMovieService';

let fakeDirectorRepository: FakeDirectorRepository;
let createDirectorService: CreateDirectorService;

let fakeMoviesRepository: FakeMoviesRepository;
let createMovieService: CreateMovieService;

describe('CreateMovie', () => {
  beforeEach(() => {
    fakeDirectorRepository = new FakeDirectorRepository();
    createDirectorService = new CreateDirectorService(fakeDirectorRepository);

    fakeMoviesRepository = new FakeMoviesRepository();
    createMovieService = new CreateMovieService(
      fakeMoviesRepository,
      fakeDirectorRepository
    );
  });

  it('should be able to create a movie', async () => {
    const createDirectorData = {
      name: 'Quentin tarantino',
    };

    const createDirector = await createDirectorService.execute(
      createDirectorData
    );

    const createMovieData = {
      name: 'Inglorious bastards',
      director_id: createDirector.id,
      gender: 'Drama',
    };

    const createMovie = await createMovieService.execute(createMovieData);

    expect(createMovie).toHaveProperty('id');
  });

  it('should not be able to create a movie with an unexistent director', async () => {
    const createDirectorData = {
      name: 'Quentin tarantino',
    };

    await createDirectorService.execute(createDirectorData);

    const createMovieData = {
      name: 'Inglorious bastards',
      director_id: 'unexistent-director',
      gender: 'Drama',
    };

    await expect(
      createMovieService.execute(createMovieData)
    ).rejects.toBeInstanceOf(AppError);
  });
});
