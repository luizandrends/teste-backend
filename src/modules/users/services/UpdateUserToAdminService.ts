import { injectable, inject } from 'tsyringe';

import AppError from '@shared/errors/AppError';
import IUsersInterface from '../interfaces/IUsersInterface';

import User from '../infra/database/entities/User';

@injectable()
class UpdateUserToAdminService {
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUsersInterface
  ) {}

  public async execute(user_id: string): Promise<User> {
    const user = await this.usersRepository.findById(user_id);

    if (!user) {
      throw new AppError('User not found', 400);
    }

    user.admin = true;

    return this.usersRepository.save(user);
  }
}

export default UpdateUserToAdminService;
