import AppError from '@shared/errors/AppError';

import FakeUsersRepository from '../interfaces/fakes/FakeUsersRepository';
import UpdateUserToAdminService from './UpdateUserToAdminService';

let fakeUsersRepository: FakeUsersRepository;
let updateUserToAdmin: UpdateUserToAdminService;

describe('UpdateUserToAdmin', () => {
  beforeEach(() => {
    fakeUsersRepository = new FakeUsersRepository();

    updateUserToAdmin = new UpdateUserToAdminService(fakeUsersRepository);
  });

  it('should be able update the usrt to admin', async () => {
    const user = await fakeUsersRepository.create({
      name: 'John Doe',
      email: 'johndoe@example.com',
      password: '123456',
    });

    const updatedUser = await updateUserToAdmin.execute(user.id);

    expect(updatedUser.admin).toBe(true);
  });

  it('should not be able update from a non-existing user', async () => {
    expect(updateUserToAdmin.execute('Non existing id')).rejects.toBeInstanceOf(
      AppError
    );
  });
});
