import { Request, Response, NextFunction } from 'express';
import AppError from '@shared/errors/AppError';

export default function ensureIsAdmin(
  request: Request,
  response: Response,
  next: NextFunction
): void {
  const authHeader = request.headers.authorization;
  if (!authHeader) {
    throw new AppError('JWT token is missing', 401);
  }
  if (request.user.admin) {
    next();
    return;
  }
  throw new AppError('Permission denied', 401);
}
