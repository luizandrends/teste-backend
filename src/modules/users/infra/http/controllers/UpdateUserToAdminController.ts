import { Request, Response } from 'express';
import { container } from 'tsyringe';
import { classToClass } from 'class-transformer';

import UpdateUserToAdminService from '@modules/users/services/UpdateUserToAdminService';

class UpdateUserToAdminController {
  public async update(request: Request, response: Response): Promise<Response> {
    const { user_id } = request.params;

    const updateUserToAdmin = container.resolve(UpdateUserToAdminService);

    const user = await updateUserToAdmin.execute(user_id);

    return response.json(classToClass(user));
  }
}

export default UpdateUserToAdminController;
