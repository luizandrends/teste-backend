import { Request, Response } from 'express';
import { container } from 'tsyringe';

import DeleteUserService from '@modules/users/services/DeleteUserService';

class DeleteUserController {
  public async delete(request: Request, response: Response): Promise<Response> {
    const user_id = request.user.id;
    const { password } = request.body;

    const deleteUser = container.resolve(DeleteUserService);

    await deleteUser.execute(user_id, password);

    return response.json({
      success: 'deleted_at updated with the current date',
    });
  }
}

export default DeleteUserController;
