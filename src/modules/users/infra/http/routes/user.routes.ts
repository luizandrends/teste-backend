import { Router } from 'express';
import { celebrate, Segments, Joi } from 'celebrate';

import UserController from '../controllers/CreateUserController';
import AuthenticateUserController from '../controllers/AuthenticateUserController';
import UpdateUserController from '../controllers/UpdateUserController';
import DeleteUserController from '../controllers/DeleteUserController';
import UpdateUserToAdminController from '../controllers/UpdateUserToAdminController';

import ensureAuthenticated from '../middlewares/ensureAuthenticated';
import ensureIsAdmin from '../middlewares/ensureIsAdmin';

const userController = new UserController();
const authenticateUserController = new AuthenticateUserController();
const updateUserController = new UpdateUserController();
const deleteUserController = new DeleteUserController();
const updateUserToAdminController = new UpdateUserToAdminController();

const userRouter = Router();

userRouter.post(
  '/create',
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required(),
      email: Joi.string().required().email(),
      password: Joi.string().required(),
    },
  }),
  userController.create
);

userRouter.post(
  '/authenticate',
  celebrate({
    [Segments.BODY]: {
      email: Joi.string().required().email(),
      password: Joi.string().required(),
    },
  }),
  authenticateUserController.create
);

userRouter.put(
  '/update',
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required(),
      email: Joi.string().email().required(),
      old_password: Joi.string(),
      password: Joi.string().min(6),
    },
  }),
  ensureAuthenticated,
  updateUserController.update
);

userRouter.delete(
  '/delete',
  ensureAuthenticated,
  celebrate({
    [Segments.BODY]: {
      password: Joi.string().min(6),
    },
  }),
  deleteUserController.delete
);

userRouter.put(
  '/admin/update/:user_id',
  celebrate({
    [Segments.PARAMS]: {
      user_id: Joi.string().required(),
    },
  }),
  ensureAuthenticated,
  ensureIsAdmin,
  updateUserToAdminController.update
);

export default userRouter;
